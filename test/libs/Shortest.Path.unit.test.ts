import ShortestPath from '../../src/libs/ShortestPath';

describe('ShortestPath', () => {
  const shortestPath = new ShortestPath();

  beforeAll(() => {
    shortestPath.addNode('GRU', {
      BRC: 10,
      CDG: 75,
      SCL: 20,
      ORL: 56,
    });
    shortestPath.addNode('BRC', {
      SCL: 5,
    });
    shortestPath.addNode('ORL', {
      CDG: 5,
    });
    shortestPath.addNode('SCL', {
      ORL: 20,
    });
  });

  it('should return the shortest path from the GRU to CDG', () => {
    expect(shortestPath.pathTo('GRU', 'CDG')).toEqual([
      'GRU',
      'BRC',
      'SCL',
      'ORL',
      'CDG',
    ]);

    shortestPath.addNode('CDG', {
      BRC: 1,
    });

    expect(shortestPath.pathTo('GRU', 'CDG')).toEqual(['GRU', 'BRC', 'CDG']);
  });

  it('should return null if destination point do not exist', () => {
    expect(shortestPath.pathTo('GRU', 'JAP')).toEqual(null);
  });

  it('should return null if origin point do not exist', () => {
    expect(shortestPath.pathTo('JAP', 'SCL')).toEqual(null);
  });

  it('should return if null origin and destination is the same', () => {
    expect(shortestPath.pathTo('SCL', 'SCL')).toEqual(null);
  });

  it('should calculate the cost for a path', () => {
    const path = ['GRU', 'BRC', 'SCL', 'ORL', 'CDG'];
    expect(shortestPath.getCost(path)).toEqual(40);
  });
});
