import PriorityQueue from '../../src/libs/PriorityQueue';

describe('PriorityQueue', () => {
  const queue = new PriorityQueue();

  beforeAll(() => {
    queue.add(5, 'Indiana');
    queue.add(6, 'Jones');
    queue.add(1, 'Lost');
    queue.add(7, 'Ark');
  });

  it('should return the items in the correct order', () => {
    expect(queue.poll()?.value).toEqual('Lost');
    expect(queue.poll()?.value).toEqual('Indiana');
    expect(queue.poll()?.value).toEqual('Jones');
    expect(queue.poll()?.value).toEqual('Ark');
    expect(queue.poll()).toEqual(null);
  });
});
