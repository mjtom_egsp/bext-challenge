import Route from '../../src/models/Route';
import RouteService from '../../src/service/RouteService';

describe('RouteService', () => {
  it('should return null when no route exists', () => {
    const routeService = new RouteService([]);

    expect(routeService.getRoute('BRA', 'JAP')).toBeNull();
  });

  it('should return the route if exists', () => {
    const routeService = new RouteService([new Route('BRA', 'JAP', 10)]);

    expect(routeService.getRoute('BRA', 'JAP')).toEqual(['BRA', 'JAP']);
  });

  it('should return the shortest route when new info inserted', () => {
    const routeService = new RouteService([new Route('BRA', 'JAP', 10)]);

    expect(routeService.getRoute('BRA', 'JAP')).toEqual(['BRA', 'JAP']);

    routeService.addRoute(new Route('BRA', 'AR', 3));
    routeService.addRoute(new Route('AR', 'JAP', 1));
    expect(routeService.getRoute('BRA', 'JAP')).toEqual(['BRA', 'AR', 'JAP']);

    routeService.addRoute(new Route('BRA', 'JAP', 1));
    expect(routeService.getRoute('BRA', 'JAP')).toEqual(['BRA', 'JAP']);
  });
});
