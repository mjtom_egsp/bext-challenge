class Route {
  from: string;
  to: string;
  cost: number;

  constructor(from: string, to: string, cost: number) {
    this.from = from;
    this.to = to;
    this.cost = cost;
  }
}

export default Route;
