import ShortestPath from '../libs/ShortestPath';
import Route from '../models/Route';

class RouteService {
  #shortestPath: ShortestPath = new ShortestPath();

  constructor(routes: Array<Route>) {
    for (const route of routes) {
      this.addRoute(route);
    }
  }

  addRoute(route: Route): void {
    this.#shortestPath.addNode(route.from, {[route.to]: route.cost});
  }

  getRoute(from: string, to: string): Array<string> | null {
    return this.#shortestPath.pathTo(from, to);
  }

  getCost(path: Array<string>): number {
    return this.#shortestPath.getCost(path);
  }
}

export default RouteService;
