import PriorityQueue from './PriorityQueue';

// Based on Dijkstra's algorithm with priority queue
class ShortestPath {
  #graph: Map<string, Map<string, number>> = new Map();

  addNode(
    name: string,
    edges: Record<string, number>,
    recursiveCall?: boolean
  ): void {
    let currentNode: Map<string, number> | undefined = this.#graph.get(name);

    if (!currentNode) {
      currentNode = new Map();
      this.#graph.set(name, currentNode);
    }

    for (const [key, value] of Object.entries(edges)) {
      currentNode.set(key, value);
      if (!recursiveCall) {
        this.addNode(key, {[name]: value}, true);
      }
    }
  }

  pathTo(start: string, end: string): Array<string> | null {
    if (this.#graph.size === 0) {
      return null;
    }

    const explored = new Set<string>();
    const frontier = new PriorityQueue<string>();
    const previous = new Map();

    const path: Array<string> = [];

    frontier.add(0, start);

    while (!frontier.isEmpty()) {
      const node = frontier.poll();

      if (node === null) {
        break;
      }

      if (node.value === end) {
        let nodeKey = node.value;
        while (previous.has(nodeKey)) {
          path.unshift(nodeKey);
          nodeKey = previous.get(nodeKey);
        }

        path.unshift(start);

        break;
      }

      explored.add(node.value);

      const edges = this.#graph.get(node.value) || new Map();
      for (const [edgeKey, edgeCost] of edges) {
        if (explored.has(edgeKey)) continue;

        if (!frontier.contains(edgeKey)) {
          previous.set(edgeKey, node.value);
          frontier.add(node.priority + edgeCost, edgeKey);
          continue;
        }

        const frontierPriority = frontier.peek(edgeKey)?.priority;
        const nodeCost = node.priority + edgeCost;

        if (frontierPriority && nodeCost < frontierPriority) {
          previous.set(edgeKey, node.value);
          frontier.add(nodeCost, edgeKey);
        }
      }
    }

    return path.length < 2 ? null : path;
  }

  getCost(path: string[]): number {
    if (path.length < 2) {
      return 0;
    }

    let totalCost = 0;
    let from = path[0];
    for (let i = 1; i < path.length; i++) {
      const to = path[i];
      totalCost += this.#graph.get(from)?.get(to) || 0;
      from = to;
    }

    return totalCost;
  }
}

export default ShortestPath;
