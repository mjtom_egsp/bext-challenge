class QueueItem<T> {
  priority: number;
  value: T;

  constructor(priority: number, value: T) {
    this.priority = priority;
    this.value = value;
  }
}

class PriorityQueue<T> {
  #items: Array<QueueItem<T>> = [];
  #keys: Set<T> = new Set();

  size(): number {
    return this.#items.length;
  }

  isEmpty(): boolean {
    return this.size() === 0;
  }

  add(priority: number, value: T): void {
    if (this.#keys.has(value)) {
      for (const item of this.#items) {
        if (item.value === value) {
          item.priority = priority;
        }
      }
    } else {
      this.#items.push(new QueueItem(priority, value));
      this.#keys.add(value);
    }
  }

  peek(value: T): QueueItem<T> | null {
    return this.#items.find(v => v.value === value) || null;
  }

  contains(value: T): boolean {
    return this.#keys.has(value);
  }

  poll(): QueueItem<T> | null {
    if (this.isEmpty()) return null;

    let minItem: QueueItem<T> = this.#items[0];
    let minIndex = 0;

    if (this.size() === 1) {
      this.#items = [];
      this.#keys.clear();
      return minItem;
    }

    for (let i = 1; i < this.#items.length; i++) {
      if (minItem.priority > this.#items[i].priority) {
        minItem = this.#items[i];
        minIndex = i;
      }
    }

    this.#items.splice(minIndex, 1);
    this.#keys.delete(minItem.value);

    return minItem;
  }
}

export default PriorityQueue;
