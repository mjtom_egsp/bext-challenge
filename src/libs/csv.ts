import * as fs from 'fs';
import * as util from 'util';

const readFileAsync = util.promisify(fs.readFile);
const writeFileAsync = util.promisify(fs.writeFile);

// this is unsafe, there is nothing guaranteeing
// that two requests made at the same time won't
// corrupt the file
const appendLine = async (line: string, inputFile: string) => {
  const data = await readFileAsync(inputFile);
  const newLine = Buffer.from('\n' + line, 'utf8');
  const newContent = Buffer.concat([data, newLine]);
  return writeFileAsync(inputFile, newContent);
};

export {appendLine};
