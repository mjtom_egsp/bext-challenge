import * as express from 'express';
import * as csv from '../libs/csv';
import Route from '../models/Route';
import RouteService from '../service/RouteService';

const getRoutes = (routeService: RouteService) => (
  req: express.Request,
  res: express.Response
) => {
  const {from, to} = req.query;

  if (!from || !to) {
    return res.status(400).json({
      message: 'Please provide the "from" and "to" parameters via query string',
    });
  }

  const paths = routeService.getRoute(from.toString(), to.toString());
  let cost = null;

  if (paths) {
    cost = routeService.getCost(paths);
  }

  return res.status(200).json({cost, paths});
};

const postRoute = (routeService: RouteService, inputFile: string) => async (
  req: express.Request,
  res: express.Response
) => {
  const {from, to, cost} = req.body;

  if (!from || !to) {
    return res.status(400).json({
      message: 'Please provide the "from" and "to" in the body',
    });
  }

  if (Number.isNaN(cost)) {
    return res.status(400).json({
      message: 'The cost parameter must be a number',
    });
  }

  const route = new Route(from, to, cost);
  routeService.addRoute(route);
  csv.appendLine(`${from},${to},${cost}`, inputFile);

  return res.sendStatus(201);
};

export {getRoutes, postRoute};
