import * as morgan from 'morgan';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as rfs from 'rotating-file-stream';
import RouteService from './service/RouteService';
import * as routesController from './controllers/routeControllers';

const start = (routeService: RouteService, inputFile: string) => {
  const accessLogStream = rfs.createStream('access.log', {
    interval: '1d',
    path: './logs',
  });

  const app = express();
  const port = 3000;

  app.use(morgan('combined', {stream: accessLogStream}));
  app.use(bodyParser.json());

  app.get('/routes', routesController.getRoutes(routeService));
  app.post('/routes', routesController.postRoute(routeService, inputFile));

  app.listen(port);
};

export {start};
