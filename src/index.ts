import * as fs from 'fs';
import * as validators from './validators/index';
import * as routeDTO from './dto/routeDTO';
import RouteService from './service/RouteService';
import * as cli from './cli';
import * as server from './server';

const init = () => {
  const inputFile = process.argv[2];

  try {
    validators.routeInputCSV(inputFile);
  } catch (err) {
    console.error('ERROR: ' + err.message);
    return;
  }

  const csvBuffer = fs.readFileSync(inputFile);
  const routes = routeDTO.csvBufferToRoutes(csvBuffer);

  const routeService = new RouteService(routes);

  server.start(routeService, inputFile);
  cli.start(routeService);
};

init();
