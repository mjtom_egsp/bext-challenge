import * as readline from 'readline';
import RouteService from './service/RouteService';

const start = (routeService: RouteService) => {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  rl.question('please enter the route: ', route => {
    const fromTo = route.split('-');

    if (fromTo.length < 2) {
      console.log('Enter an valid route, ex: GRU-CGD');
      return start(routeService);
    }

    const from = fromTo[0].toUpperCase();
    const to = fromTo[1].toUpperCase();
    const paths = routeService.getRoute(from, to);
    if (paths === null) {
      console.log('Route not found');
      return start(routeService);
    }

    const cost = routeService.getCost(paths);
    console.log(`best route: ${paths.join(' - ')} > $${cost}`);

    return start(routeService);
  });
};

export {start};
