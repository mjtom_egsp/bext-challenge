import * as fs from 'fs';

const routeInputCSV = (inputFile: string) => {
  if (!inputFile) {
    throw new Error('Please provide an input file');
  }

  if (!fs.existsSync(inputFile)) {
    throw new Error('The input file does not exist');
  }
};

export {routeInputCSV};
