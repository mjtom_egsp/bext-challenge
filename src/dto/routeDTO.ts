import Route from '../models/Route';

const csvBufferToRoutes = (csvBuffer: Buffer): Array<Route> => {
  const csvData = csvBuffer.toString();

  const rows = csvData
    .split('\n')
    .filter((row: string) => row !== '')
    .map((row: string) => row.trim().split(','));

  const routes: Array<Route> = [];
  for (const [from, to, cost] of rows) {
    const parsedFrom = from.toUpperCase();
    const parsedTo = to.toUpperCase();
    const parsedCost = Number(cost);

    if (Number.isNaN(parsedCost)) {
      throw new Error('The cost value must be a number');
    }

    routes.push(new Route(parsedFrom, parsedTo, parsedCost));
  }

  return routes;
};

export {csvBufferToRoutes};
