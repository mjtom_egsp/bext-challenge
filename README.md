# Bext Challenge

Aplicação para calcular o trajeto mais barato entre dois aeroportos.

## Dependências

- [Node.js ^v12.18.3](https://nodejs.org/en/)

## Instalação

Para instalar o projeto na sua maquina, execute o seguinte comando:

```bash
tar xvf bext-challenge.tar.gz
cd bext-challenge/
npm install
```

## Executando o projeto

Para executar o projeto execute o seguinte comando:

```bash
npm start input-sample.csv
```

Caso queira, substitua o argumento `input-sample.csv` com um argumento proprio que contenha rotas.

Na execução deste comando será inicializado a API na porta 3000 e o CLI que ficara disponivel no terminal para consultas.

## Rotas da API

### GET /routes

Retorna a rota e o seu custo baseado nos parametros `from` e `to` que são fornecidos via querystring.

> GET /routes?from=GRU&to=BRC

```json
{
  "cost": 40,
  "paths": ["GRU", "BRC", "SCL", "ORL", "CDG"]
}
```

### POST /routes

Cria uma nova rota baseando nos parametros `from`, `to` e `cost` que são enviados no payload. Também registra esse no valor no CSV que foi utilizado para inicializar a aplicação.

> POST /routes

```json
{
  "from": "JAP",
  "to": "AUR",
  "cost": 2
}
```

## Sobre a Aplicação

O algoritmo utilizado para encontrar o caminho mais barato foi construindo utilizando como base o algoritmo de Edsger Wybe Dijkstra. Este é um algoritmo conhecido na literatura para conseguir encontrar o menor caminho em grafos que possuem pesos positivos.

Se a aplicação fosse construida para a produção, haveria mais detalhes que incrementaria neste projeto, por exemplo:

- Dockerização da aplicação
- Fluxo de CI/CD
- Melhoria no monitoramento da aplicação (APM)
- Ter a infraestrutura da aplicação no código
- Ter uma analise da qualidade do código que está sendo construido (ex: SonarQube)
